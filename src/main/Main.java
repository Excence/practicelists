package main;

public class Main {
    public static void main(String[] args) {
        MyList myList = new MyList();
        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList._getSize();
        myList.addStart("15"); myList.intoString();
        String[] arr = {"16" , "17", "18", "19", "20"};
        myList.addArray(arr); myList.intoString();
        myList.remove("18"); myList.intoString();
        myList.replace("16", "33"); myList.intoString();
        myList.addAfter("33", "34"); myList.intoString();
        myList.deleteStart(); myList.intoString();
        myList.deleteEnd(); myList.intoString();
        myList.deleteAll(); myList.intoString();

    }
}
