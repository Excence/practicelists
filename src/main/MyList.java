package main;

public class MyList {

    private Node _first;
    private int _size;

    public MyList() {
        _first = null;
        _size = 0;
    }

    public int _getSize() {
        return _size;
    }

    public void addStart(String data){
        Node node = new Node(data);
        node.setNext(_first);
        _first = node;
    }

    public void add(String data) {
        Node current = _first;
        if(_first == null){
            _first = new Node(data);
            _size++;
            return;
        }
        while (current.getNext() != null) {
            current = current.getNext();
        }

        current.setNext(new Node(data));
        _size++;

    }

    public void addArray(String[] arrString){
        for (String str: arrString) {
            add(str);
        }
    }

    public void remove(String data) {
        Node current = _first;
        Node next = current.getNext();

        if (_first.getData().equals(data)) {
            if (_size == 1) {
                _first.setData(null);
                _size--;
                return;
            }
            _first.setData(null);
            _first = _first.getNext();
            _size--;
            return;
        }

        while (next != null) {
            if (next.getData().equals(data)) {
                current.setNext(next.getNext());
                next = null;
                _size--;
                return;
            }
            current = next;
            next = current.getNext();
        }
    }

    public void replace(String oldData, String newData){
        Node current = _first;

        while (current.getNext() != null){
            if (current.getData().equals(oldData)){
                current.setData(newData);
                return;
            }
            current = current.getNext();
        }
    }

    public void addAfter(String data, String newData){
        Node current = _first;

        while (current.getNext() != null){
            if (current.getData().equals(data)){
                Node newNode = new Node(newData);
                newNode.setNext(current.getNext());
                current.setNext(newNode);
                return;
            }
            current = current.getNext();
        }
    }

    public void deleteAll(){
        _first = null;
        _size = 0;
    }

    public void deleteStart(){
        _first = _first.getNext();
        _size--;
    }

    public void deleteEnd(){
        Node curremt = _first;
        Node next = curremt.getNext();

        while (next.getNext() != null){
            curremt = next;
            next = curremt.getNext();
        }

        curremt.setNext(null);
    }

    public void intoString(){
        if (_size == 0){
            System.out.print("Список пустой");
            return;
        }

        Node current = _first;
        Node next = current.getNext();
        System.out.print("Список : {");

            while (next != null){
                System.out.print(current.getData() + ", ");
                current = next;
                next = current.getNext();
            }
        System.out.println(current.getData()+"}");
    }
}
