package main;

public class Node {

    private String _data;
    private Node _next;

    public Node(String data) {
        _data = data;
        _next = null;
    }

    public void setData(String data) {
        _data = data;
    }

    public String getData() {
        return _data;
    }

    public void setNext(Node next) {
        _next = next;
    }

    public Node getNext() {
        return _next;
    }
}